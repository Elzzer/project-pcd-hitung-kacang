i=imread('knewww.jpg');

img = im2double(i);
avg3 = ones(2)/3;
k1 = imfilter(img, avg3,'conv');

igray=rgb2gray(k1);
igamma=imadjust(igray,[],[],0.4);
se=strel('cube',1);
bg=imopen(igamma,se);
sisibg=edge(bg,'log');

%for c=1:3
%    erot= imerode(dilat,se2);
%    dilat = imdilate(erot,se2);
%    dilat = imdilate(dilat,se2);
%end
bersih=bwareaopen(sisibg,300);
bersihFill=imfill(bersih,'holes');

se2 = strel('line',11,90);
%dilat = imdilate(bersihFill,se2);
%for c=1:6
%    dilat = imdilate(dilat,se2);
%end
%erot= imerode(dilat,se2);
%erot= imerode(erot,se2);
%erot= imerode(erot,se2);
%imshow(dilat);
dilat = imdilate(bersihFill,se2);
dilat = imdilate(dilat,se2);
dilat = imdilate(dilat,se2);
erot= imerode(dilat,se2);
erot= imerode(erot,se2);
%dilat = imdilate(erot,se2);
%dilat = imdilate(dilat,se2);

label=bwlabel(erot,4);
coloredLabels = label2rgb (label, 'hsv', 'k', 'shuffle');
%tbl = countEachLabel(coloredLabels)

stats = regionprops(erot);

hasilkacang = 0;
temp = "";

imshow(erot);

arrayKacang = [1,2,3,4,5,6,7,8,9,10,11,12,13];
for c = 2:length(stats)-1
    temp = stats(c).BoundingBox;
    hitungan = temp(3) * temp(4);
    if c == 2
        koinPembagi = (temp(3)*temp(4)/2.8);
        %disp("pembagi : " + hitungan);
    else
        rectangle('Position',[temp(1),temp(2),0,temp(4)],'EdgeColor','r','LineWidth',2);
        rectangle('Position',[temp(1),temp(2),temp(3),0],'EdgeColor','r','LineWidth',2);
    
        hitunganF = hitungan / koinPembagi;
        %disp(hitunganF);
        if hitunganF > 1.95
            arrayKacang(c) = 3;
        elseif hitunganF < 0.9
            arrayKacang(c) = 1;
        else
            arrayKacang(c) = 2;
        end;
    end;
end

stats = struct2table(stats);
totalKacang = 0;
for kk = 3:height(stats)-1
    %text(stats.Centroid(kk,1)-25, stats.Centroid(kk,2)-10,'K:');
    text(stats.Centroid(kk,1), stats.Centroid(kk,2)-10,num2str(arrayKacang(kk)));
    totalKacang = totalKacang + arrayKacang(kk);
end

text(0, 10,'total kacang : ','Color','y');
text(210,15,num2str(totalKacang),'Color','y');
