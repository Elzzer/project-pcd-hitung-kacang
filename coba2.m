i = imread('knew.jpg');
i = rgb2gray(i);
se = strel('line',11,90);
tepi = edge(i,'sobel');
dilat = imdilate(tepi,se);
for c=1:6
    dilat = imdilate(dilat,se);
end

erot= imerode(dilat,se);
for c=1:25
    erot = imerode(erot,se);
end
dilat = imdilate(erot,se);
for c=1:10
    dilat = imdilate(dilat,se);
end

%Kmed = medfilt2(i);
[centers,radii] = imfindcircles(i,[30,100],'ObjectPolarity','Dark', ... ,
    'Sensitivity',0.95);
imshow(i);
h = viscircles(centers,radii);

